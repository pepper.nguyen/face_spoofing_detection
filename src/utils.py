import cv2
import numpy as np
import os
from matplotlib import pyplot as plt
import logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(message)s')


def desparity(img_l, img_r):
    img_l = cv2.cvtColor(img_l, cv2.COLOR_RGB2GRAY)
    img_r = cv2.cvtColor(img_r, cv2.COLOR_RGB2GRAY)
    stereo = cv2.StereoBM_create(numDisparities=16, blockSize=15)
    disparity = stereo.compute(img_l, img_r)
    plt.imshow(disparity, 'gray')
    plt.show()


def get_xml_opencv():
    cv2_base_dir = os.path.dirname(os.path.abspath(cv2.__file__))
    haar_file = os.path.join(cv2_base_dir, 'data/haarcascade_frontalface_default.xml')
    haar_eyer = os.path.join(cv2_base_dir, 'data/haarcascade_righteye_2splits.xml')
    haar_eyel = os.path.join(cv2_base_dir, 'data/haarcascade_leftteye_2splits.xml')
    haar_eye_open = os.path.join(cv2_base_dir, 'data/haarcascade_eye_tree_eyeglasse.xml')
    haar_eye = os.path.join(cv2_base_dir, 'data/haarcascade_eye.xml')
    face_cascade = cv2.CascadeClassifier(haar_file)
    eye_cascade = cv2.CascadeClassifier(haar_eye)
    eye_open_cascade = cv2.CascadeClassifier(haar_eye_open)
    eyer_cascade = cv2.CascadeClassifier(haar_eyer)
    eyel_cascade = cv2.CascadeClassifier(haar_eyel)
    return face_cascade, eye_cascade, eye_open_cascade, eyel_cascade, eyer_cascade


fc, ec, eoc, elc, erc = get_xml_opencv()


def face_detect_crop(cam):
    while True:
        ret, img = cam.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = fc.detectMultiScale(gray, 1.3, 5)
        for (x, y, w, h) in faces:
            roi_gray = gray[y:y + h, x:x + w]
            roi_color = img[y:y + h, x:x + w]
            cv2.rectangle(img, (x, y), (x + w, y + h))
            eyes = eoc.detectMultiScale(roi_gray, scaleFactor=1.1,
                                        minNeighbors=5, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
            for (ex, ey, ew, eh) in eyes:
                cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
                # cropped = img[y:y + h, x:x + w]
                # cv2.imwrite("{0}_{1}_{2}_{3}_{4}.jpg".format(img_name, x, y, w, h), cropped)
        cv2.imshow('aloha', img)
        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break


def record_save():
    cam = cv2.VideoCapture(0)
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter("data/videos/fake.mp4", fourcc, 15, (int(cam.get(3)), int(cam.get(4))))
    while cam.isOpened():
        ret, frame = cam.read()
        if ret:
            # frame = cv2.flip(frame, 0)
            cv2.imshow('frame', frame)
            out.write(frame)
            if cv2.waitKey(30) & 0xFF == ord('q'):
                break
        else:
            break
    out.release()
    cam.release()
    cv2.destroyAllWindows()
# record_save()


def create_dataset_detector(inp_video, dataset_path, cf_detector_folder, thresh, nb_skipped_frames):
    logging.debug("LOADING FACE DETECTOR...")
    proto_path = os.path.join(cf_detector_folder, 'deploy.prototxt')
    detector_path = os.path.join(cf_detector_folder, 'res10_300x300_ssd_iter_140000.caffemodel')
    net = cv2.dnn.readNetFromCaffe(prototxt=proto_path, caffeModel=detector_path)
    iv = cv2.VideoCapture(inp_video)
    count = 0
    save_index = 0
    item_name = inp_video[inp_video.rfind("/") + 1:][:-4]
    vid_type = 'real' if "real" in inp_video else 'fake'
    while True:
        ret, frame = iv.read()
        if not ret:
            break
        if vid_type == "real":
            frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
        count += 1
        if count % nb_skipped_frames != 0:
            continue
        h, w = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
                                     (300, 300), (104.0, 177.0, 123.0))
        # pass blob through network
        net.setInput(blob)
        detections = net.forward()
        # at least 1 face found
        if len(detections) > 0:
            i = np.argmax(detections[0, 0, :, 2])  # take the bbox largest proba
            confidence = detections[0, 0, i, 2]
            if confidence > thresh:
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                start_x, start_y, end_x, end_y = box.astype("int")
                face = frame[start_y:end_y, start_x:end_x]
                save_name = "dataset/" + vid_type + "/" + item_name + "_" + str(save_index) + ".png"
                cv2.imwrite(os.path.join(dataset_path, save_name), face)
                save_index += 1
                logging.debug("Saved to disk: {0}".format(save_name))
    iv.release()
    cv2.destroyAllWindows()
# create_dataset_detector("data/videos/fake.mp4", "data/", "face_detector/", 0.7, 4)

# def create_dataset_opencv(inp_video, dataset_path):
#     iv = cv2.VideoCapture(inp_video)
#     save_index = 0
#     item_name = inp_video[inp_video.rfind("/") + 1:][:-4]
#     vid_type = 'real' if "real" in inp_video else 'fake'
#     while True:
#         ret, frame = iv.read()
#         frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
#         if not ret:
#             break
#         gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#         faces = fc.detectMultiScale(gray, 1.3, 5)
#         for (x, y, w, h) in faces:
#             cropped = frame[y:y + h, x:x + w]
#             save_name = "dataset/" + vid_type + "/" + item_name + "_" + str(save_index) + ".png"
#             cv2.imwrite(os.path.join(dataset_path, save_name), cropped)
#             print(os.path.join(dataset_path, save_name))
#             save_index += 1
#         k = cv2.waitKey(30) & 0xff
#         if k == 27:
#             break
#     iv.release()
#     cv2.destroyAllWindows()
# create_dataset_opencv("data/videos/real.mp4", "data/")


import pickle
from src.models.liveness_model import LivenessModel
from keras.preprocessing.image import img_to_array

proto_path = 'face_detector/deploy.prototxt'
detector_path = 'face_detector/res10_300x300_ssd_iter_140000.caffemodel'
net = cv2.dnn.readNetFromCaffe(prototxt=proto_path, caffeModel=detector_path)
le = pickle.loads(open("models/le.pkl", "rb").read())


def video_spoof_detection(vid_path, thresh):
    cam = cv2.VideoCapture(vid_path)
    # global outputFrame, lock
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    v_width = int(cam.get(3))
    v_height = int(cam.get(4))
    out_path = vid_path.replace("uploads", "downloads")
    print(v_width, v_height)
    out = cv2.VideoWriter(out_path, fourcc, 15, (v_width, v_height))
    model = LivenessModel.build(width=32, height=32, nb_chan=3, nb_class=len(le.classes_))
    model.load_weights("ckpt/liveness-weights-48-0.10.hdf5")
    while cam.isOpened():
        ret, frame = cam.read()
        # frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
        if not ret:
            break
        # frame = cv2.resize(frame, (600, v_height))
        (h, w) = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
                                     (300, 300), (104.0, 177.0, 123.0))
        net.setInput(blob)
        detections = net.forward()
        print(frame.shape)
        if len(detections) > 0:
            for i in range(0, detections.shape[2]):
                confidence = detections[0, 0, i, 2]
                if confidence > thresh:
                    box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                    x_start, y_start, x_end, y_end = box.astype("int")
                    # extract the face ROI
                    face = frame[y_start:y_end, x_start:x_end]
                    face = cv2.resize(face, (32, 32))
                    face = face.astype("float") / 255.0
                    face = img_to_array(face)
                    face = np.expand_dims(face, axis=0)

                    # model predict

                    preds = model.predict(face)[0]
                    j = np.argmax(preds)
                    label = le.classes_[j]
                    # draw the label and bbox
                    label = "{}: {:.4f}".format(label, preds[j])
                    cv2.putText(frame, label, (x_start, y_start - 10),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                    cv2.rectangle(frame, (x_start, y_start), (x_end, y_end),
                                  (0, 0, 255), 2)
        out.write(frame)
        print("==========")
    out.release()
    cam.release()
    cv2.destroyAllWindows()
video_spoof_detection("../data/uploads/fake_1.mp4", 0.7)
