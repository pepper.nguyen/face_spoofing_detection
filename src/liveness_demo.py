from imutils.video import VideoStream
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import numpy as np
import imutils
import logging
import pickle
import time
import cv2
import os


def demo(cf_detector_folder, model_path, le_path, thresh):
    logging.debug("LOADING FACE DETECTOR...")
    proto_path = os.path.join(cf_detector_folder, 'deploy.prototxt')
    detector_path = os.path.join(cf_detector_folder, 'res10_300x300_ssd_iter_140000.caffemodel')
    net = cv2.dnn.readNetFromCaffe(prototxt=proto_path, caffeModel=detector_path)
    # logging.debug("[INFO] loading liveness detector...")
    print("[INFO] loading liveness detector...")
    model = load_model(model_path)
    le = pickle.loads(open(le_path, "rb").read())
    print("[INFO] starting video stream...")
    vs = VideoStream(src=0).start()
    # warm up cam after start
    time.sleep(2.0)
    while True:
        frame = vs.read()
        frame = imutils.resize(frame, width=600)
        # grab the frame dimensions and convert it to a blob
        h, w = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
                                     (300, 300), (104.0, 177.0, 123.0))
        net.setInput(blob)
        detections = net.forward()
        # loop over the detections
        for i in range(0, detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            if confidence > thresh:
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                x_start, y_start, x_end, y_end = box.astype("int")
                # dimensions of frame
                x_start = max(0, x_start)
                y_start = max(0, y_start)
                x_end = min(w, x_end)
                y_end = min(h, y_end)
                # extract the face ROI
                face = frame[y_start:y_end, x_start:x_end]
                face = cv2.resize(face, (32, 32))
                face = face.astype("float") / 255.0
                face = img_to_array(face)
                face = np.expand_dims(face, axis=0)

                # pass the face ROI through the trained liveness detector

                # model predict
                preds = model.predict(face)[0]
                j = np.argmax(preds)
                label = le.classes_[j]

                # draw the label and bbox
                label = "{}: {:.4f}".format(label, preds[j])
                cv2.putText(frame, label, (x_start, y_start - 10),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
                cv2.rectangle(frame, (x_start, y_start), (x_end, y_end),
                              (0, 0, 255), 2)

        cv2.imshow("aloha", frame)
        key = cv2.waitKey(1) & 0xFF

        if key == ord("q"):
            break

    # do a bit of cleanup
    cv2.destroyAllWindows()
    vs.stop()


if __name__ == '__main__':
    demo("face_detector/", "models/model_liveness.h5", "models/le.pkl", 0.8)
