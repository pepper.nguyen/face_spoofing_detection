import keras
from keras.models import Sequential, load_model
from keras.layers import Conv2D, Flatten, Dense, MaxPooling2D, BatchNormalization, Dropout, Activation
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras import backend as K
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import classification_report
from imutils import paths
import numpy as np
import logging
import pickle
import cv2
import os
import matplotlib
matplotlib.use("Agg")

from matplotlib import pyplot as plt


LEARN_RATE = 1e-4
NB_EPOCH = 50
BATCH_SIZE = 8


class LivenessModel:
    @staticmethod
    def build(width, height, nb_chan, nb_class):
        input_shape = (height, width, nb_chan)
        chan_dim = -1
        if K.image_data_format() == "channels_first":
            input_shape = (nb_chan, height, width)
            chan_dim = 1
        model = Sequential()
        model.add(Conv2D(16, (3, 3), padding="same", input_shape=input_shape))
        model.add(BatchNormalization(axis=chan_dim))
        model.add(Activation("relu"))
        model.add(Conv2D(16, (3, 3), padding="same"))
        model.add(BatchNormalization(axis=chan_dim))
        model.add(Activation("relu"))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Conv2D(32, (3, 3), padding="same"))
        model.add(BatchNormalization(axis=chan_dim))
        model.add(Activation("relu"))
        model.add(Conv2D(32, (3, 3), padding="same"))
        model.add(BatchNormalization(axis=chan_dim))
        model.add(Activation("relu"))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Flatten())
        model.add(Dense(64))
        model.add(Activation("relu"))
        model.add(BatchNormalization())
        model.add(Dropout(0.5))

        model.add(Dense(nb_class, activation='softmax'))
        print(model.summary())
        return model


def get_prepared(dataset_folder):
    data = []
    labels = []
    list_img_paths = paths.list_images(dataset_folder)
    for x in list_img_paths:
        image = cv2.imread(x)
        image = cv2.resize(image, (32, 32))
        data.append(image)
        label = x.split(os.path.sep)[-2]
        labels.append(label)
    data = np.array(data, dtype="float") / 255.0
    return data, labels


if __name__ == '__main__':
    le = LabelEncoder()
    dat, labels = get_prepared("..\\data\\dataset\\")
    print(len(labels))
    print(len(dat))
    lab = le.fit_transform(labels)
    lab = np_utils.to_categorical(lab, 2)
    print(le.classes_)
    with open("le.pkl", "wb") as f:
        f.write(pickle.dumps(le))
    x_train, x_test, y_train, y_test = train_test_split(dat, lab, test_size=0.2, random_state=42)
    augment = ImageDataGenerator(rotation_range=20, zoom_range=0.15,
                                 width_shift_range=0.2, height_shift_range=0.2,
                                 shear_range=0.15, horizontal_flip=True, fill_mode="nearest")
    model = LivenessModel.build(width=32, height=32, nb_chan=3, nb_class=len(le.classes_))
    logging.debug("COMPILING MODEL ...")
    model.compile(optimizer=keras.optimizers.Adam(lr=LEARN_RATE, decay=LEARN_RATE / NB_EPOCH),
                  loss="binary_crossentropy", metrics=['acc'])
    weight_f = "../ckpt/liveness-weights-{epoch:02d}-{val_loss:.2f}.hdf5"
    model_ckpt = ModelCheckpoint(weight_f, monitor='val_loss', save_best_only=True, mode='min')
    early_stop = EarlyStopping(patience=7, monitor='val_loss')
    # logging.debug("Training network for {} epochs ...".format(NB_EPOCH))
    print("Training network for {} epochs ...".format(NB_EPOCH))
    history = model.fit_generator(augment.flow(x_train, y_train, batch_size=BATCH_SIZE),
                                  validation_data=(x_test, y_test),
                                  steps_per_epoch=len(x_test) // BATCH_SIZE,
                                  epochs=NB_EPOCH, callbacks=[model_ckpt, early_stop])
    # Save model
    logging.debug("SAVING MODEL ...")
    model.save("model_liveness.h5")
    del model
    # Load model
    loaded_model = load_model("model_liveness.h5")
    # Evaluate
    logging.debug("STARTING EVALUATION PROCESS ...")
    preds = loaded_model.predict(x_test, batch_size=BATCH_SIZE)
    print(classification_report(y_test.argmax(axis=1), preds.argmax(axis=1), target_names=le.classes_))
    # Plot training
    logging.debug("DRAW PLOT TRAINING")
    plt.plot(np.arange(0, NB_EPOCH), history.history["loss"], label="train_loss")
    plt.plot(np.arange(0, NB_EPOCH), history.history["val_loss"], label="val_loss")
    plt.plot(np.arange(0, NB_EPOCH), history.history["acc"], label="train_acc")
    plt.plot(np.arange(0, NB_EPOCH), history.history["val_acc"], label="val_acc")
    plt.title("Training Plot on Loss and Accuracy")
    plt.xlabel("Epoch #")
    plt.ylabel("Loss/Accuracy")
    plt.legend(loc="lower left")
    plt.savefig("plot training.png")