from keras.preprocessing.image import img_to_array
from src.models.liveness_model import LivenessModel
from imutils.video import VideoStream
from flask import Response
from flask import Flask
from flask import request, redirect
from flask import send_from_directory
from flask import render_template
import numpy as np
from flask import url_for
import pickle
import threading
from werkzeug.utils import secure_filename
import dlib
# import argparse
# from flask import jsonify
import imutils
import time
import cv2
import os
import tensorflow as tf

# NUM_PARALLEL_EXEC_UNITS = 4
# config = tf.ConfigProto(intra_op_parallelism_threads=NUM_PARALLEL_EXEC_UNITS, inter_op_parallelism_threads=2,
#                         allow_soft_placement=True, device_count={'CPU': NUM_PARALLEL_EXEC_UNITS})
# session = tf.Session(config=config)
# K.set_session(session)
# os.environ["OMP_NUM_THREADS"] = "4"
# os.environ["KMP_BLOCKTIME"] = "30"
# os.environ["KMP_SETTINGS"] = "1"
# os.environ["KMP_AFFINITY"] = "granularity=fine,verbose,compact,1,0"
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


# initialize the output frame and a lock to ensure thread-safe
outputFrame = None
lock = threading.Lock()

UPLOAD_DIR = "data/uploads/"
DOWNLOAD_DIR = "data/downloads/"

app = Flask("Face Spoofing Detection")
app.config['UPLOAD_FOLDER'] = UPLOAD_DIR
app.config['DOWNLOAD_FOLDER'] = DOWNLOAD_DIR

if not os.path.exists(UPLOAD_DIR):
    os.makedirs(UPLOAD_DIR)
if not os.path.exists(DOWNLOAD_DIR):
    os.makedirs(DOWNLOAD_DIR)

vs = VideoStream(src=0).start()


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/stream", methods=["POST"])
def stream():
    if request.method == "POST":
        return render_template("stream.html")


@app.route("/upload", methods=["GET", "POST"])
def upload():
    if request.method == "POST":
        f = request.files['file']
        if f.filename == '':
            print('No file selected')
            return redirect(request.url)
        if f:
            filename = secure_filename(f.filename)
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            return redirect(url_for('output', filename=filename))
    return render_template("upload.html")


@app.route("/result/<filename>", methods=["GET", "POST"])
def output(filename):
    if request.method == "POST":
        return send_from_directory(app.config['DOWNLOAD_FOLDER'], filename, as_attachment=True)
    else:
        eval_status = video_spoof_detection(os.path.join(app.config["UPLOAD_FOLDER"], filename))
        return render_template("result.html", result=eval_status, fname=filename)


def video_spoof_detection(vid_path):
    cam = cv2.VideoCapture(vid_path)
    print("11111")
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    v_width = int(cam.get(3))
    v_height = int(cam.get(4))
    out_path = vid_path.replace("uploads", "downloads")
    out = cv2.VideoWriter(out_path, fourcc, 20, (v_width, v_height))
    detector = dlib.get_frontal_face_detector()
    graph1 = tf.Graph()
    f_count = 0
    total_count = 0
    print("22222")
    with graph1.as_default():
        session1 = tf.Session()
        with session1:
            model = LivenessModel.build(width=32, height=32, nb_chan=3, nb_class=len(le.classes_))
            model.load_weights("src/ckpt/liveness-weights-48-0.10.hdf5")
            while cam.isOpened():
                ret, frame = cam.read()
                total_count += 1
                # frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
                if not ret:
                    break
                rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                dets = detector(rgb_image)
                for det in dets:
                    det_left = max(0, det.left())
                    det_top = max(0, det.top())
                    det_right = min(v_width, det.right())
                    det_bot = min(v_height, det.bottom())
                    face = frame[det_top:det_bot, det_left:det_right]
                    face = cv2.resize(face, (32, 32))
                    face = face.astype("float") / 255.0
                    face = img_to_array(face)
                    face = np.expand_dims(face, axis=0)
                    # model predict
                    preds = model.predict(face)[0]
                    j = np.argmax(preds)
                    label = le.classes_[j]
                    if label == "fake":
                        f_count += 1
                    # draw the label and bbox
                    label = "{}: {:.4f}".format(label, preds[j])
                    cv2.putText(frame, label, (det_left, det_top - 10),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                    cv2.rectangle(frame, (det_left, det_top), (det_right, det_bot),
                                  (0, 0, 255), 2)
                out.write(frame)
    out.release()
    cam.release()
    cv2.destroyAllWindows()
    print(f_count)
    print(total_count)
    print(f_count/total_count)
    final_res = "SPOOFED" if f_count / total_count > 0.6 else "REAL"
    return final_res


def stream_make_prediction():
    global vs, outputFrame, lock
    time.sleep(2.0)
    graph = tf.Graph()
    with graph.as_default():
        sess = tf.Session()
        with sess:
            model = LivenessModel.build(width=32, height=32, nb_chan=3, nb_class=len(le.classes_))
            model.load_weights("src/ckpt/liveness-weights-48-0.10.hdf5")
            detector = dlib.get_frontal_face_detector()
            while True:
                frame = vs.read()
                frame = imutils.resize(frame, width=600)
                rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                dets = detector(rgb_image)
                for det in dets:
                    det_left = max(0, det.left())
                    det_top = max(0, det.top())
                    det_right = min(700, det.right())
                    det_bot = min(480, det.bottom())
                    face = frame[det_top:det_bot, det_left:det_right]
                    face = cv2.resize(face, (32, 32))
                    face = face.astype("float") / 255.0
                    face = img_to_array(face)
                    face = np.expand_dims(face, axis=0)
                    # model predict
                    preds = model.predict(face)[0]
                    j = np.argmax(preds)
                    label = le.classes_[j]
                    # draw the label and bbox
                    label = "{}: {:.4f}".format(label, preds[j])
                    cv2.putText(frame, label, (det_left, det_top - 10),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                    cv2.rectangle(frame, (det_left, det_top), (det_right, det_bot),
                                  (0, 0, 255), 2)
                with lock:
                    outputFrame = frame


def generate():
    global outputFrame, lock
    while True:
        with lock:
            # check if the output frame is available, otherwise skip
            # the iteration of the loop
            if outputFrame is None:
                continue
            (flag, encoded_image) = cv2.imencode(".jpg", outputFrame)
            # ensure the frame was successfully encoded
            if not flag:
                continue
        yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' +
               bytearray(encoded_image) + b'\r\n')


@app.route("/video_feed")
def video_feed():
    return Response(generate(),
                    mimetype="multipart/x-mixed-replace; boundary=frame")


# check to see if this is the main thread of execution
if __name__ == '__main__':
    # ap = argparse.ArgumentParser()
    # ap.add_argument("-o", "--port", type=int, required=True,
    #                 help="ephemeral port number of the server (1024 to 65535)")
    # args = vars(ap.parse_args())

    le = pickle.loads(open("src/models/le.pkl", "rb").read())
    print(le.classes_)
    t = threading.Thread(target=stream_make_prediction)
    t.daemon = True
    t.start()

    # app.run(host='0.0.0.0', port=args["port"], debug=True,
    #         threaded=True, use_reloader=False)
    app.jinja_env.cache = {}
    app.run(host='0.0.0.0', port=4706, debug=True,
            threaded=True, use_reloader=False)
vs.stop()
